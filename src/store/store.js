import { createStore } from 'vuex';

export default createStore({
    state: {
        dateBackend: [],
        currentCard: {}
    },
    actions: { 
        async DateFromBackend(ctx) {
            try {
                const res = await fetch('https://back.miktadov.com/api/yana/productions/');
                let dateBackend = await res.json();
                console.log(dateBackend);
                ctx.commit('updateDateBackend', dateBackend);
            } catch (error) {
                console.error("Ошибка при получении данных с бэкенда:", error);
            }
        }
    },
    mutations: {
        updateDateBackend(state, dateBackend) {
            state.dateBackend = dateBackend;
        },
        setCurrentCard(state, card) {
            state.currentCard = card;
        }
    },
    getters: {
        DateBackend(state) {
            return state.dateBackend;
        },
        getCurrentCard(state) {
            return state.currentCard;
        }
    },
});