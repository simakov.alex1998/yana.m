    import './assets/main.css'
    import Vue3TouchEvents from 'vue3-touch-events';
    import { createApp } from 'vue'
    import App from './App.vue'
    import router from './router'
    import store from './store/store'

    const app = createApp(App)

    app.use(router)
    app.use(store)
    app.use(Vue3TouchEvents);
    app.mount('#app')
